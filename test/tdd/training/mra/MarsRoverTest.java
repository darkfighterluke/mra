package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testObstacleIsProperlyInserted() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		assertTrue(rover.planetContainsObstacleAt(4, 7));
	}
	
	@Test(expected=MarsRoverException.class)
	public void testNegativePlanetCoordinatesShouldRaiseException() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		new MarsRover(-1, -1, planetObstacles);
	}
	
	@Test(expected=MarsRoverException.class)
	public void testObstaclesNotOnPlanetShouldRaiseException() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		new MarsRover(1, 2, planetObstacles);
	}
	
	@Test(expected=MarsRoverException.class)
	public void testObstaclesOnNegativeCoordinatesShouldRaiseException() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(-4,7)");
		planetObstacles.add("(2,-3)");
		new MarsRover(10, 10, planetObstacles);
	}
	
	@Test
	public void testEmptyCommandStringShouldReturn00N() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		assertEquals(MarsRover.INITIAL_ROVER_COORDINATES, rover.executeCommand(""));
	}
	
	@Test
	public void testCommandStringrShouldReturn00E() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	
	@Test
	public void testCommandStringrShouldReturn00S() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("r");
		assertEquals("(0,0,S)", rover.executeCommand("r"));
	}
	
	@Test
	public void testCommandStringrShouldReturn00W() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("r");
		rover.executeCommand("r");
		assertEquals("(0,0,W)", rover.executeCommand("r"));
	}
	
	@Test
	public void testCommandStringrShouldReturn00N() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("r");
		rover.executeCommand("r");
		rover.executeCommand("r");
		assertEquals("(0,0,N)", rover.executeCommand("r"));
	}
	
	@Test
	public void testCommandStringlShouldReturn00W() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	
	@Test
	public void testCommandStringlShouldReturn00S() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("l");
		assertEquals("(0,0,S)", rover.executeCommand("l"));
	}
	
	@Test
	public void testCommandStringlShouldReturn00E() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("l");
		rover.executeCommand("l");
		assertEquals("(0,0,E)", rover.executeCommand("l"));
	}
	
	@Test
	public void testCommandStringlShouldReturn00N() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("l");
		rover.executeCommand("l");
		rover.executeCommand("l");
		assertEquals("(0,0,N)", rover.executeCommand("l"));
	}
	
	@Test
	public void testCommandStringfShouldReturn01N() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,1,N)", rover.executeCommand("f"));
	}
	
	@Test
	public void testCommandStringfShouldReturn10E() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("r");
		assertEquals("(1,0,E)", rover.executeCommand("f"));
	}
	
	@Test
	public void testCommandStringfShouldReturn22E() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("r");
		rover.executeCommand("f");
		assertEquals("(2,2,E)", rover.executeCommand("f"));
	}

	@Test
	public void testCommandStringfShouldReturn21S() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("r");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("r");
		assertEquals("(2,1,S)", rover.executeCommand("f"));
	}
	
	@Test
	public void testCommandStringbShouldReturn48E() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("r");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		rover.executeCommand("f");
		assertEquals("(4,8,E)", rover.executeCommand("b"));
	}
	
	@Test
	public void testCombinedCommandStringffrffShouldReturn22E() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
	
	@Test
	public void testShouldReturn22E() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
	
	@Test
	public void testCommandStringbWithBackWrappingShouldReturn09N() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,9,N)", rover.executeCommand("b"));
	}
	
	@Test
	public void testCommandStringfWithUpperWrappingShouldReturn00N() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,N)", rover.executeCommand("ffffffffff"));
	}
	
	@Test
	public void testCommandStringfWithRightWrappingShouldReturn00E() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,E)", rover.executeCommand("rffffffffff"));
	}
	
	@Test
	public void testCommandStringfWithLeftWrappingShouldReturn90W() throws Exception {
		List<String> planetObstacles= new ArrayList<String>();
		MarsRover rover=new MarsRover(10, 10, planetObstacles);
		assertEquals("(9,0,W)", rover.executeCommand("lf"));
	}
	
}
