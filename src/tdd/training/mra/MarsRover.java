package tdd.training.mra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MarsRover {
	private int planetX;
	private int planetY;
	private List<Integer[]> planetObstacles;
	
	private int currentRoverX;
	private int currentRoverY;
	private char currentRoverOrientation;
	
	public final static String INITIAL_ROVER_COORDINATES="(0,0,N)";	
	List<Character> FLATTEN_WIND_ROSE= Arrays.asList('N', 'E', 'S', 'W');
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		if(planetX < 0 || planetY<0) throw new MarsRoverException();
		this.planetObstacles=new ArrayList<Integer[]>();
		for(String obstacle : planetObstacles) {
			Integer[] coordinates=this.parsePlanetObstaclesCoordinates(obstacle);
			if(coordinates[0]>planetX-1 || coordinates[0]<0) throw new MarsRoverException();
			if(coordinates[1]>planetY-1 || coordinates[1]<0) throw new MarsRoverException();
			this.planetObstacles.add(coordinates);
		}
		
		this.planetX=planetX;
		this.planetY=planetY;
		
		this.currentRoverX=0;
		this.currentRoverY=0;
		this.currentRoverOrientation=FLATTEN_WIND_ROSE.get(0);
	}
	
	private Integer[] parsePlanetObstaclesCoordinates(String obstacle) throws MarsRoverException {
		obstacle=obstacle.replace("(", "");
		obstacle=obstacle.replace(")", "");
		String[] obstacleCoordinates=obstacle.split(",");
		int x=Integer.parseInt(obstacleCoordinates[0]);
		int y=Integer.parseInt(obstacleCoordinates[1]);
		Integer[] coordinates= {x,y};
		return coordinates;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		Integer[] passedCoordinates= {x, y};
		for(Integer[] coordinates : planetObstacles) {
			if(coordinates[0]==passedCoordinates[0] && coordinates[1]==passedCoordinates[1]) return true;
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		if(commandString=="") {
			return INITIAL_ROVER_COORDINATES;
		}
		for(int i=0; i<commandString.length(); i++) {
			if(commandString.charAt(i)=='r' || commandString.charAt(i)=='l') {
				setRoverOrientation(commandString.charAt(i));
			}else if(commandString.charAt(i)=='f' || commandString.charAt(i)=='b') {
				setRoverPosition(commandString.charAt(i));
			}
		}
		return '('+Integer.toString(currentRoverX)+','+Integer.toString(currentRoverY)+','+currentRoverOrientation+')';
	}
	
	private void setRoverOrientation(char commandString) {
		if(commandString=='r') {
			int currentWindRoseIndex=FLATTEN_WIND_ROSE.indexOf(currentRoverOrientation);
			if(currentWindRoseIndex<3) {
				currentRoverOrientation=FLATTEN_WIND_ROSE.get(currentWindRoseIndex+1);
			}else if(currentWindRoseIndex==3) {
				currentRoverOrientation=FLATTEN_WIND_ROSE.get(0);
			}
		}else if(commandString=='l') {
			int currentWindRoseIndex=FLATTEN_WIND_ROSE.indexOf(currentRoverOrientation);
			if(currentWindRoseIndex>0) {
				currentRoverOrientation=FLATTEN_WIND_ROSE.get(currentWindRoseIndex-1);
			}else if(currentWindRoseIndex==0) {
				currentRoverOrientation=FLATTEN_WIND_ROSE.get(3);
			}
		}
	}
	
	private void setRoverPosition(char commandString) throws MarsRoverException {
		if(commandString=='f') {
			if(currentRoverOrientation=='N') {
				if(currentRoverY==planetY-1) {
					currentRoverY=0;
				}else {
					currentRoverY++;
				}
			}else if(currentRoverOrientation=='E') {
				if(currentRoverX==planetX-1) {
					currentRoverX=0;
				}else {
					currentRoverX++;
				}
			}else if(currentRoverOrientation=='W') {
				if(currentRoverX==0) {
					currentRoverX=planetX-1;
				}else {
					currentRoverX--;
				}
			}else if(currentRoverOrientation=='S') {
				if(currentRoverY==0) {
					currentRoverY=planetY-1;
				}else {
					currentRoverY--;
				}
			}
		}else if (commandString=='b') {
			if(currentRoverOrientation=='N') {
				if(currentRoverY==0) {
					currentRoverY=planetY-1;
				}else {
					currentRoverY--;
				}
			}else if(currentRoverOrientation=='E') {
				if(currentRoverX==0) {
					currentRoverX=planetX-1;
				}else {
					currentRoverX--;
				}
			}else if(currentRoverOrientation=='W') {
				if(currentRoverX==planetX-1) {
					currentRoverX=0;
				}else {
					currentRoverX++;
				}
			}else if(currentRoverOrientation=='S') {
				if(currentRoverY==planetY-1) {
					currentRoverY=0;
				}else {
					currentRoverY++;
				}
			}
		}
	}

}
